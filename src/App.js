import React, { Component } from 'react';
import './App.css';
import Bhaskar from './bhaskar';
import CustomTextInput from './textinput';
import AutoFocusTextInput from './gopal';

class App extends Component {
  constructor(){
    super();
    this.userRef=React.createRef();
  }
editval(){
  //console.log(this.userRef)
  this.userRef.current.focus()
}
  render() {
    return (
     <div className="App"> 
       <h3>what is react ref</h3>
       <input type="text" name="user" ref={this.userRef} />
       <button onClick={()=>this.editval()}>Click Me</button>
       <Bhaskar/>
       <CustomTextInput/>
       <AutoFocusTextInput/>
     </div>
    );
  }

}

export default App;
